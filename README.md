# README/Quickstart #
This is the readme for the PocketMediaNativeAds Plugin for Unity. Its a library that brings the PocketMediaNative ads to Unity.

#What does the project contain?
The repo contains two projects: 
* NativeAdsLibrary: implementation of the Pocket Media NativeAds library to download them and display them in iOS and Android. 
* TappyPlane: based on the [sample code by Anwell Wang](http://anwell.me/articles/unity3d-flappy-bird/).

#How do I start?
Import the PocketMediaNativeAds.plugin into your Unity project. 

#iOS 
Build the https://bitbucket.org/pocketbrain/nativeadslib-ios project and import the (drag and drop) PocketMediaNativeAds.framework and PocketMediaNativeAds.bundle into your Xcode project from the root directory of your SDK folder

Select Project Settings > Build Phases
Expand "Link Binary with Libraries" section
For each framework, click "+", and add the framework to the Project

For further usage see the nativeadslib-ios documentation.

#Android
After importing the PocketMediaNativeAds.plugin you can use the provided C# classes which are wrappers calling native code. The usage is the same as the Android library. You implement the NativeAdRequestListener interface which then can be used to start an NativeAdRequest. Finally there is the NativeAdOpener to open ads in an embedded webview.
 
The android library documentation can be found at: https://bitbucket.org/pocketbrain/androidnativeadslib