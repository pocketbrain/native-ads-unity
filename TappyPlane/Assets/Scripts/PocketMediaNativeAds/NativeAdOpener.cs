﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class NativeAdOpener
	{
		AndroidJavaObject opn;
		private static readonly NativeAdOpener _nativeAdOpener = new NativeAdOpener();

		public static NativeAdOpener Instance {
			get{
				return _nativeAdOpener;
			}
		}


		static NativeAdOpener(){
		}

		private NativeAdOpener ()
		{
			AndroidJavaClass opener = new AndroidJavaClass ("mobi.pocketmedia.nativeadslib.NativeAdOpener");
			opn = opener.CallStatic<AndroidJavaObject> ("getInstance");
		}

		public void openAdInEmbeddedWebBrowser(NativeAdUnit adUnit){

			// Get Current Activity
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
			AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");

			AndroidJavaObject unit = new AndroidJavaObject ("mobi.pocketmedia.nativeadslib.NativeAdUnit");

			unit.Call ("setCampaignName", adUnit.campaignName);
			unit.Call ("setCampaignDescription", adUnit.campaignDescription);
			unit.Call ("setClickURL", adUnit.clickUrl );
			unit.Call ("setCampaignImage", adUnit.campaignImage );




			opn.Call ("openAdInEmbeddedWebBrowser", new object[] {
				jo , unit
			}); 
		}
	}
}

