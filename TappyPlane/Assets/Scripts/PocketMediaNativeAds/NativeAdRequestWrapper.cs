﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class NativeAdRequestWrapper 
	{
		AndroidJavaObject nativeAdsClass;
		NativeAdsRequestListenerWrapper c_b;

		public NativeAdRequestWrapper(int NumOfAds, int AffiliateId, int YourPlacement, NativeAdsRequestListener listener){

			// Get Current Activity
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
			AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");


			// Construct NativeAdsRequest
			nativeAdsClass = new AndroidJavaObject ("mobi.pocketmedia.nativeadslib.NativeAdRequest", jo, NumOfAds, AffiliateId);
			c_b = new NativeAdsRequestListenerWrapper (listener);

		}

		public void initAdRequest(){
			nativeAdsClass.Call("initAdRequest", new object[] {
				c_b
			});
		}




			

	}
}

