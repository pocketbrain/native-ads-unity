﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace AssemblyCSharp
{
	public class NativeAdsRequestListenerWrapper : AndroidJavaProxy
	{

		private NativeAdsRequestListener listener;


		public NativeAdsRequestListenerWrapper (NativeAdsRequestListener listener) : base("mobi.pocketmedia.nativeadslib.NativeAdsRequestListener"){
			this.listener = listener;
		}


		/**
     * Method to be implemented when the adRequest has started.
     * @param request a NativeAdRequest object
     */
		void onAdRequestStarted(AndroidJavaObject request){
			Debug.Log ("AdRequest has been started");
			listener.onAdRequestStarted (null);

		}

		/**
     * Method to be implemented when the adRequest is successful.
     * @param request a NativeAdRequest object
     * @param ads a list of NativeAdUnit objects
     */
		void onAdRequestSuccess(AndroidJavaObject request, AndroidJavaObject ads){
			Debug.Log ("Adrequest has been a succes");

			int count = ads.Call<int> ("size");

			Debug.Log ("The count of the Array is count: " + count);

			List<NativeAdUnit> list = new List<NativeAdUnit> ();

			for (int i = 0; i < count; i++) {
				list.Add (new NativeAdUnit (ads.Call<AndroidJavaObject> ("get", i)));

			}

			listener.onAdRequestSuccess (null, list);
				

		}

		/**
     * Method to be implemented when the adRequest has failed.
     * @param request a NativeAdRequest object
     * @param error an exception to catch
     */
		void onAdRequestError(AndroidJavaObject request, AndroidJavaObject exception){
			Debug.Log ("Its all over for this AdRequest");

			listener.onAdRequestError (null, new Exception ());
		}


}
		
	public class NativeAdUnit
	{	
		public string campaignName;
		public string campaignDescription;
		public string campaignImage;
		public string clickUrl;

		public NativeAdUnit(AndroidJavaObject nativeadunit){
			campaignName = nativeadunit.Get<string> ("campaignName");
			campaignDescription = nativeadunit.Get<string> ("campaignDescription");
			campaignImage = nativeadunit.Get<string> ("campaignImage");
			clickUrl = nativeadunit.Get<string> ("clickURL");
		}
	}

}

