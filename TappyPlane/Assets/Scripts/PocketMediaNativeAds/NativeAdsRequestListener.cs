﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public interface NativeAdsRequestListener
	{

		void onAdRequestStarted(NativeAdRequest request);

		void onAdRequestSuccess(NativeAdRequest request, List<NativeAdUnit> ads);

		void onAdRequestError(NativeAdRequest request, Exception exception);
	}
}

