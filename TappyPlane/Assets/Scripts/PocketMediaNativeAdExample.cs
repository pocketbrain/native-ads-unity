﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp
{
	public class PocketMediaNativeAdExample : MonoBehaviour, NativeAdsRequestListener
	{
		private GUIStyle fontsStyle;

		public GameObject canvas;
		public GameObject nativeAdImage;
		public GameObject nativeAdTitle;
		public Text myText;
		public Image myImage;
		public bool showed = false;
		public bool gettedAds = false;
		public bool image = true;
		public bool opened = false;

		NativeAdRequest nativeadsRequest;
		List<NativeAdUnit> nativeads;

		void Start(){
			nativeadsRequest = new NativeAdRequest (3, 3, 3, this);

			canvas = GameObject.Find ("Canvas");
			nativeAdImage = new GameObject ("nativeAdImage");

			nativeAdImage.transform.SetParent (canvas.transform);

			nativeAdTitle = new GameObject ("nativeAdTitle");
			nativeAdTitle.transform.SetParent (canvas.transform);


			fontsStyle = new GUIStyle();
			fontsStyle.fontSize = 20;

			setupUI ();
		}

		void setupUI() {
			myText = nativeAdTitle.AddComponent<Text> ();
			myText.transform.SetParent (nativeAdTitle.transform);
			Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
			myText.font = ArialFont;
			myText.material = ArialFont.material;

			myImage = nativeAdImage.AddComponent<Image> ();
			myImage.transform.SetParent (nativeAdImage.transform);
			myImage.rectTransform.sizeDelta = new Vector2 (800, 300);

			Button b = gameObject.GetComponent<Button> ();
			b.onClick.AddListener (delegate() {
				NativeAdOpener.Instance.openAdInEmbeddedWebBrowser(nativeads[0]);
			});

		}

		// Update the Model in this Method
		void Update () {
			if (!showed) {
				nativeadsRequest.initAdRequest ();
				showed = true;
			}
		}

		// Update the GUI In this Method
		void OnGUI()
		{	
			myImage.rectTransform.localPosition = new Vector3 (100, 0);

			if (gettedAds && image) {
				StartCoroutine(
					downloadImg (nativeads [0].campaignImage)
				);

				image = true;

			}

			if (gettedAds || !opened) {
				Button button = canvas.GetComponentInChildren<Button> ();
				button.onClick.AddListener (delegate {
					NativeAdOpener.Instance.openAdInEmbeddedWebBrowser (nativeads [0]);	
				});

				opened = true;
			}
		}

		public IEnumerator downloadImg(string url)
		{
			WWW imageurl = new WWW (url);
			yield return imageurl;


			//Simple check to see if there's indeed a texture available
			if (imageurl.texture != null) {

				//Construct a new Sprite
				Sprite sprite = new Sprite ();     

				//Create a new sprite using the Texture2D from the url. 
				//Note that the 400 parameter is the width and height. 
				//Adjust accordingly
				sprite = Sprite.Create (imageurl.texture, new Rect (0, 0, imageurl.texture.width, imageurl.texture.height), Vector2.zero);  

				//Assign the sprite to the Image Component
				myImage.rectTransform.sizeDelta = new Vector2(imageurl.texture.width, imageurl.texture.height);
				myImage.sprite = sprite;  
			}
		} 


		public void onAdRequestStarted(NativeAdRequest request){
			Debug.Log ("Adrequest Started");
		}

		public void onAdRequestSuccess(NativeAdRequest request, List<NativeAdUnit> ads){
			Debug.Log ("Adrequest Succesful");

			foreach(NativeAdUnit x in ads){
				Debug.Log (x.campaignImage);
				Debug.Log (x.campaignDescription);
				Debug.Log (x.campaignName);
				Debug.Log (x.clickUrl);
			}

			nativeads = ads;
			gettedAds = true;

		}

		public void onAdRequestError(NativeAdRequest request, Exception exception){
			Debug.Log ("Adrequest Failed");
		}
	}
}

