﻿using Newtonsoft.Json.Linq;
using System;
using System.Net;
using UnityEngine;

namespace NativeAdsLibrary
{
    class NativeAds
    {
        private static Action initAdRequest;
        private static JArray nativeAdsArray;

        static void Main(string[] args)
        {
            NativeAds nativeAds = new NativeAds();
            initAdRequest = new Action(nativeAds.InitAdRequest);
            initAdRequest.Invoke();
            Console.ReadLine();
        }

        public void InitAdRequest() {
            //Task<JArray> task = NativeAdsAsync();
            //task.Start();
            //task.Wait();
            GetNativeAds();
            
        }

        public JArray GetNativeAds() {
            JArray jArray = new JArray();
            WebClient webClinet = new WebClient();
            var data = webClinet.DownloadString(GetServerUrl());

            try
            {
                jArray = JArray.Parse(data);
                // JObject o = JObject.Parse(data);

                for (int i = 0; i < jArray.Count; i++)
                {
                    Console.WriteLine("Campaign name: " + jArray[i]["campaign_name"]);
                    Console.WriteLine();
                    Console.WriteLine("Campaign description: " + jArray[i]["campaign_description"]);
                    Console.WriteLine();
                    Console.WriteLine("Campaign image: " + jArray[i]["campaign_image"]);
                    Console.WriteLine();
                    Console.WriteLine("Click url: " + jArray[i]["click_url"]);
                }
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return jArray;
        }

        public static string GetServerUrl()
        {
            string UrlRequest = "http://offerwall.beta.pmgbrain.com/ow.php?output=json&os=android&version=4.4.2&model=LG-D802&token=1596461d-b0fe-437f-854b-675a3eede776&affiliate_id=100023&limit=3";
            return (UrlRequest);
        }

        //async Task<JArray> NativeAdsAsync() {
        //    JArray jArray = new JArray();
        //    WebClient webClinet = new WebClient();
        //    var data = webClinet.DownloadString("http://offerwall.beta.pmgbrain.com/ow.php?output=json&os=android&version=4.4.2&model=LG-D802&token=1596461d-b0fe-437f-854b-675a3eede776&affiliate_id=100023&limit=3");

        //    try
        //    {
        //        jArray = JArray.Parse(data);
        //        // JObject o = JObject.Parse(data);

        //        for (int i = 0; i < jArray.Count; i++)
        //        {
        //            Console.WriteLine("Campaign name: " + jArray[i]["campaign_name"]);
        //            Console.WriteLine();
        //            Console.WriteLine("Campaign description: " + jArray[i]["campaign_description"]);
        //            Console.WriteLine();
        //            Console.WriteLine("Campaign image: " + jArray[i]["campaign_image"]);
        //            Console.WriteLine();
        //            Console.WriteLine("Click url: " + jArray[i]["click_url"]);
        //        }
        //        Console.ReadLine();

        //        Task<JArray> nativeAdsTask = NativeAdsAsync();
        //        JArray nativeAds = await nativeAdsTask;

        //        return jArray;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.Message);
        //        return jArray; 
        //    }
        //}
    }
}
